//
//  HeroAPI.swift
//  consultr-challengerTests
//
//  Created by Juan Manuel Iturriza on 16/03/2022.
//

@testable import consultr_challenger

enum HeroApiMock: HeroApiProtocol {

    case ERROR_DECODING
    case ERROR_NO_DATA
    case SUCCESS
    case SUCCESS_EMPTY
    
    func fetchHeroes(successClosure: @escaping (([Hero]) -> ()), errorClosure: @escaping ((String?) -> ())) {
        switch self {
        case .ERROR_DECODING:
           break
        case .ERROR_NO_DATA:
            break
        case .SUCCESS:
            break
        case .SUCCESS_EMPTY:
            successClosure([])
        }
    }
    
}
