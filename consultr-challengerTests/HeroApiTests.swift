//
//  HeroApiTests.swift
//  consultr-challengerTests
//
//  Created by Juan Manuel Iturriza on 17/03/2022.
//

import XCTest
@testable import consultr_challenger

class HeroApiTests: XCTestCase {

    func testInitializer_whenUrlIsValid_noErrorIsTriggered(){
        XCTAssertNoThrow(try HeroAPI())
    }
    
    func testInitializer_whenUrlIsInvalid_errorIsTriggered(){
        XCTAssertThrowsError(try HeroAPI(apiEndpoint: "invalid URL"))
    }

    func testInitializer_whenUrlIsEmpty_errorIsTriggered(){
        XCTAssertThrowsError(try HeroAPI(apiEndpoint: ""))
    }
    
    func testApi_whenSucess_successClosureIsCalled(){
        let sut = HeroApiMock.SUCCESS_EMPTY
        let expectation = expectation(description: "success closure")
        var isClosureInvoked = false
        
        sut.fetchHeroes { _ in
            isClosureInvoked = true
            expectation.fulfill()
        } errorClosure: { _ in }

        wait(for: [expectation], timeout: 2)
        XCTAssertTrue(isClosureInvoked)
        
    }
    
}
