//
//  Hero.swift
//  consultr-challenger
//
//  Created by Juan Manuel Iturriza on 16/03/2022.
//

import UIKit

struct Hero: Decodable, Hashable {
    let id: Int
    let name: String
    let height: String
    let weight: String
    let image: String
}

// MARK: Decoding implementation
extension Hero {
    
    enum RootKeys : String, CodingKey {
        case id, name, appearance, images
    }
    
    enum AppearanceKeys : String, CodingKey {
        case height, weight
    }
    
    enum ImagesKeys : String, CodingKey {
        case md
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
    
        let appearanceContainer = try container.nestedContainer(keyedBy: AppearanceKeys.self, forKey: .appearance)
        height = try appearanceContainer.decode([String].self, forKey: .height)[1]
        weight = try appearanceContainer.decode([String].self, forKey: .weight)[1]
        
        let imageContainer = try container.nestedContainer(keyedBy: ImagesKeys.self, forKey: .images)
        image = try imageContainer.decode(String.self, forKey: .md)
    }
}
