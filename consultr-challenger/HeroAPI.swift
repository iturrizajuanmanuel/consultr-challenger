//
//  HeroAPI.swift
//  consultr-challenger
//
//  Created by Juan Manuel Iturriza on 16/03/2022.
//

import Alamofire

protocol HeroApiProtocol{
    func fetchHeroes(successClosure : @escaping (([Hero]) -> ()), errorClosure : @escaping ((String?)-> ()))
}

enum AppError : Error {
    case ApiEndpointNotFound
    case InvalidUrlError
}

class HeroAPI {

    private var apiEndpoint : String
    
    init(apiEndpoint : String? = nil) throws {
        guard let apiEndpoint = apiEndpoint ?? Bundle.main.object(forInfoDictionaryKey: "API_endpoint") as? String else {
            throw AppError.ApiEndpointNotFound
        }
        guard URL(string: apiEndpoint) != nil else {
            throw AppError.InvalidUrlError
        }
        self.apiEndpoint = apiEndpoint
    }
    
    func fetchHeroes(successClosure : @escaping (([Hero]) -> ()), errorClosure : @escaping ((String?)-> ())){
        AF.request(apiEndpoint).response { response in
            guard let data = response.data else {
                errorClosure(Bundle.main.localizedString(forKey: "API_NO_DATA", value: nil, table: nil))
                return
            }

            guard let result = try? JSONDecoder().decode([Hero].self, from: data) else {
                errorClosure(Bundle.main.localizedString(forKey: "API_DECODING_ERROR", value: nil, table: nil))
                return
            }

            successClosure(result)
        }

    }
    
}
