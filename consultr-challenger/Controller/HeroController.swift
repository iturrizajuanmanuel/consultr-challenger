//
//  HeroController.swift
//  consultr-challenger
//
//  Created by Juan Manuel Iturriza on 16/03/2022.
//

import Foundation

protocol HeroControllerDelegate : AnyObject {
    func showHeroes(_ heroArray : [Hero])
    func showError(error : String)
}

class HeroController {
    weak var delegate: HeroControllerDelegate?
    var heroes: [Hero]?
    
    func fetchHeroes() {
        guard let api = try? HeroAPI() else {
            delegate?.showError(error: "HeroApi couldnt be instanciated")
            return
        }
        
        api.fetchHeroes { [weak self] result in
            self?.heroes = result
            self?.delegate?.showHeroes(result)
        } errorClosure: { [weak self] error in
            let errorString = error ?? Bundle.main.localizedString(forKey: "GENERIC_ERROR", value: nil, table: nil)
            self?.delegate?.showError(error: errorString)
        }

    }
    
}
