//
//  HeroCollectionViewController.swift
//  consultr-challenger
//
//  Created by Juan Manuel Iturriza on 15/03/2022.
//

import UIKit


class HeroCollectionViewController: UICollectionViewController {

    private let cellID = "HeroCellId"
    private var controller: HeroController!
    private var datasource : UICollectionViewDiffableDataSource<Int, Hero>?
    private let indicator = UIRefreshControl()
    
    // MARK: - Initializers
    init() {
        super.init(collectionViewLayout: UICollectionViewLayout())
        controller = HeroController()
        controller.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Superhero App"
        initializeSearchBar()
        initilalizeCollectionView()
        controller.fetchHeroes()
    }

    private func initilalizeCollectionView() {
        collectionView.addSubview(indicator)
        indicator.addTarget(self, action: #selector(refreshCollectionData), for: .valueChanged)
        indicator.beginRefreshing()
        
        self.collectionView!.register(UINib(nibName: "HeroCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellID)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        layout.itemSize = CGSize(width: (collectionView.frame.width / 2.3) , height: 300)
        collectionView.setCollectionViewLayout(layout, animated: true)
    
        datasource = UICollectionViewDiffableDataSource(collectionView: collectionView, cellProvider: { collectionView, indexPath, hero in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellID, for: indexPath) as! HeroCollectionViewCell
            cell.setData(hero)
            return cell
        })
        collectionView.dataSource = datasource
    }
    
    @objc func refreshCollectionData(){
        indicator.endRefreshing()
        datasource?.apply(NSDiffableDataSourceSnapshot())
        controller.fetchHeroes()
    }
}

// MARK: - SearchBar implementation
extension HeroCollectionViewController: UISearchBarDelegate {

    private func initializeSearchBar(){
        let searchController = UISearchController()
        searchController.searchBar.delegate = self
        self.navigationItem.searchController = searchController
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("new query: \(searchText)")
        let filteredItems : [Hero]?
        if searchText.isEmpty {
            filteredItems = controller.heroes
        } else {
            filteredItems = controller.heroes?.filter{ $0.name.contains(searchText) }
        }
        
        showHeroes(filteredItems ?? [])
    }
    
}

// MARK: - HeroControllerDelegate implementation
extension HeroCollectionViewController: HeroControllerDelegate {
    
    func showError(error: String) {
        print("Error: \(error)")
    }
    
    func showHeroes(_ heroArray: [Hero]) {
        indicator.endRefreshing()
        
        var snapshot = NSDiffableDataSourceSnapshot<Int, Hero>()
        snapshot.appendSections([1])
        snapshot.appendItems(heroArray, toSection: 1)
        datasource?.apply(snapshot)
    }
    
}
