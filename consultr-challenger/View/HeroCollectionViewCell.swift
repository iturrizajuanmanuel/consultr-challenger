//
//  HeroCollectionViewCell.swift
//  consultr-challenger
//
//  Created by Juan Manuel Iturriza on 15/03/2022.
//

import UIKit
import Kingfisher

class HeroCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
        
    func setData(_ hero: Hero){
        nameLabel.text = hero.name
        heightLabel.text = hero.height
        weightLabel.text = hero.weight
        
        if let url = URL(string: hero.image){
            imageView.kf.setImage(with: url)
        }
    }

}
